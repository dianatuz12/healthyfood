const {src, dest, series, watch} = require('gulp')
const fileinclude = require('gulp-file-include');
const connect = require("gulp-connect");
const sass = require("gulp-sass")
const appPath = {
    scss: './app/scss/**/*.scss',
    js: './app/js/*.js',
    img: [
        './app/img/**/*.jpg',
        './app/img/**/*.png'
    ]
}

function html() {
    return src('./app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('./dist/')
           );
}

function server() {
    connect.server({
        name: 'Dev App',
        root: 'dist',
        port: 8012,
        livereload: true
    })
}
function scss(){
    return src('./app/scss/main.scss')
    return src(appPath.scss)
        .pipe(scss({
            outputStyle: 'compressed'
        }))
        .pipe(dest('./dist/css')
        )
}
function image() {
    return src(appPath.img)
        .pipe(imagemin())
        .pipe(rename(function (path) {
            return {
                dirname: path.dirname,
                basename: path.basename.substr(0, 3) + "-test",
                extname: path.extname
            };
        }))
        .pipe(dest(destPath.img))
        .pipe(connect.reload())
}


watch(appPath.scss, scss)
watch(appPath.img, {events: 'add'}, image);
watch("app/**/*.html", html);
exports.default = series(html, server, scss, image)